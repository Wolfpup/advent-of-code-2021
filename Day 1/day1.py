with open("Day 1/day1 input.txt") as f:
    content = f.read().splitlines()

 # part one- count how many times the depth increases

def count_changes(values):
    increases = 0
    previous = values[0]

    for v in values:
        increases+= 1 if int(v) > int(previous) else 0
        previous = v
    return increases

increases = count_changes(content)

print(increases)


#part two- create sliding windows and compare the sums of their values instead

#create the windows
windows = []
for i in range(len(content)):

    #we only care about 3 length windows
    window = content[i : i+3]
    if len(window) == 3:
        #convert to int
        r = [int(i) for i in window]
        windows.append(r)
    
#we could have simply created the sums while iterating but lets be neat for no real reason
sums = []

for w in windows:
    sums.append(sum(w))

#now do the same check but on this list
print (count_changes(sums))
