with open("Day 6/Day6 input.txt") as f:
    inputs = f.read()


#I admit I came into this puzzle having a little knowledge, namely, that we will be dealing with
#huge numbers of lanternfish (subreddit memes to thank for that). So, we will turn to numpy for 
#efficient handling of huge arrays

import numpy as np
from collections import Counter


def advance_day():

    global fishies

    # #'generate' n new fish
    new_fish = [8] *np.count_nonzero(fishies == 0)

    #each day a fish counts down one
    f = lambda x: (x -1)%7 if x<= 7 else x-1
    vf = np.vectorize(f)
    fishies =vf(fishies)

    # #add the new fish to our collection
    fishies = np.append(fishies,new_fish)
    

#get our initial fish
fishies = np.fromstring(inputs, dtype=int, sep=',')

for _ in range(256):
    advance_day()
print(fishies.size)



#part two. Technically we could run part 1 and come back later, but lets 
#do it faster and smarter

#instead of tracking fish individually, we'll group them based on time remaining
#note that we are using int64 as normal ints would overflow!
fishies = np.array([0,0,0,0,0,0,0,0,0],dtype='int64')

initalCounts = Counter(inputs.split(','))

#populate our array
for ele in initalCounts.most_common():

    fishies[int(ele[0])] = int(ele[1])


def advance_day():
    global fishies
    
    #take note of how many fish are ready to spawn
    new_fish_count = fishies[0]
    #we will roll the array left one space
    fishies =np.roll(fishies,-1)

    #rolling the array will move 0 fish to 8, but they need to be set to 6 instead
    #so take those 8 fish and add them to 6
    fishies[6] +=fishies[8]
    #any zero fish spawn new fish starting at 8, so add what we counted earlier
    fishies[8] = new_fish_count
    
for _ in range(256):
    advance_day()
print(np.sum(fishies))