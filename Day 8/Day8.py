with open("Day 8/Day8 input.txt") as f:
    inputs = f.read().splitlines()

from collections import Counter

#wrangle some data
pairs = []

for line in inputs:
    pair = line.split("|")
    pairs.append([y.split() for y in pair])


#focus only on the digits that use 1 4 7 and 8 - they use unique segment patterns
#   2,4,3 and 7 respectively

#first we just flatten the values
digits = []
for pair in pairs:
    digits +=[x for x in pair[1]]

#get the length of each digit
lens = [len(x) for x in digits]

#count the occurances
count = Counter(lens)

#print the sum of the unique segments
print(count[2]+count[4]+count[3]+count[7])




#part two

    ##our unscrambled representations for what a digit should be. Useful later
    #                  "abcefg" :0,
    #                  "cf":1,
    #                  "acdeg":2,
    #                  "acdfg":3,
    #                  "bcdf":4,
    #                 "abdfg":5,
    #                 "abdefg":6,
    #                 "acf":7,
    #                 "abcdefg":8,
    #                 "abcdfg":9}

#given the input, return a dictionary of corrected segments
def decode_placements(input):

    mappings = {}
    #start with the unique ones
    one=    next(x for x in input if len(x) ==2)
    four =  next(x for x in input if len(x)==4)
    seven = next(x for x in input if len(x)==3)
    eight = next(x for x in input if len(x) ==7)

    #given these we can start mapping things

    #the top bar is whichever segment seven has that one does not
    mappings['a']= [item for item in seven if item not in one].pop()

    #the bottom bar is whichever segment is in 2,3 and 5 but *not* in 4 and 7
    #so, get the 5 length digits
    fivers = [x for x in input if len(x)==5]
    #find the bars
    bars =set(fivers[0])&set(fivers[1])&set(fivers[2])

    #top and middle bars 
    tm = (bars&set(seven)) | (bars&set(four))

    #we can now map the bottom bar
    mappings["g"] = (bars-tm).pop()

    #and the middle bar
    mappings["d"] = (bars & set(four)).pop()

    #from there, the only remaining bar in 4 is the top left
    mappings["b"] = (set(four) - (set(one)|bars)).pop()

    #four and bar segments make 9
    all = set(["a","b","c","d","e","f","g"])

    nine = set(four) | bars

    #difference between nine and 8 is the bottom left
    mappings["e"] = (all - nine).pop()

    #we know that the display for 6 is the only six-element display that contains all our currently known

    known = set(mappings.values())
    sixers = [x for x in input if len(x)==6]
    six = set([x for x in sixers if known.issubset(set(x))]).pop()

    #it also contains the bottom right
    mappings["f"] = (set(six) - known).pop()

    #which means we know the top right too
    mappings["c"] = (set(one) - set(mappings["f"])).pop()


    #we now have our mappings
    return mappings



def create_map(mapping):
    digit_map = {}

    digit_map[0] = mapping["a"]+mapping["b"]+mapping["c"]+mapping["e"]+mapping["f"]+mapping["g"]
    digit_map[1] = mapping["c"]+mapping["f"]
    digit_map[2] = mapping["a"]+mapping["c"]+mapping["d"]+mapping["e"]+mapping["g"]
    digit_map[3] = mapping["a"]+mapping["c"]+mapping["d"]+mapping["f"]+mapping["g"]
    digit_map[4] = mapping["b"]+mapping["c"]+mapping["d"]+mapping["f"]
    digit_map[5] = mapping["a"]+mapping["b"]+mapping["d"]+mapping["f"]+mapping["g"]
    digit_map[6] = mapping["a"]+mapping["b"]+mapping["d"]+mapping["e"]+mapping["f"]+mapping["g"]
    digit_map[7] = mapping["a"]+mapping["c"]+mapping["f"]
    digit_map[8] = mapping["a"]+mapping["b"]+mapping["c"]+mapping["d"]+mapping["e"]+mapping["f"]+mapping["g"]
    digit_map[9] = mapping["a"]+mapping["b"]+mapping["c"]+mapping["d"]+mapping["f"]+mapping["g"]



    return digit_map


def decode_digit(digit,mapping):

 
    display_map = create_map(mapping)
    #invert the dictionary cause I'm dumb
    inv_map = {v: k for k, v in display_map.items()}

    
    for d in inv_map.keys():
        if set(d) == set(digit):
            return inv_map[d] 



values = []
for p in pairs:
    mapping = decode_placements(p[0])
    display = ""
    for d in p[1]:
        val =decode_digit(d,mapping)
        display+=str(val)
    values.append(int(display))


print(sum(values))