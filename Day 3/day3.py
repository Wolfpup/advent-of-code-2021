from collections import Counter

with open("Day 3/day3 input.txt") as f:
    inputs = f.read().splitlines()


#each binary number is 12 digits long
bin_len = len(inputs[0])

columns = [0]*bin_len

#reorder our data into columns
for c in range(bin_len):

    columns[c] = [b[c] for b in inputs]


#count the occurences
counters = [Counter(col) for col in columns]


#calculate gamma - the most common bit in each column
gamma = [0]*bin_len
for i in range(bin_len):
    gamma[i] = counters[i].most_common(1)[0][0]

gamma = ''.join(gamma)

#calculate epsilon - the least common bit in each column
epsilon = [0]*bin_len
for i in range(bin_len):
    epsilon[i] = counters[i].most_common()[-1][0]

epsilon = ''.join(epsilon)



#convert from binary to base 2 and multiply together for fuel consumption
print(int(gamma,2)*int(epsilon,2))


#part two. Since we have to recalculate the bit counts frequently, we will make a function this time

#return a list of counts for each bit column
def get_bit_counts(input_list):

    cols = [0]*bin_len
    #reorder our data into columns
    for c in range(bin_len):

        cols[c] = [b[c] for b in input_list]
     #count the occurences
    return [Counter(col) for col in cols]
    



#a number passes this criteria if the given bit is the most common
def oxygen_criteria(bin_num,index):

    counters = get_bit_counts(candidates)
    
    #there is a gotcha here. Instead of simply returning true if it has the most common value, we have to check
    #to see if there is a tie. In that case, we only return true if there is a 1 here
    sorted = counters[index].most_common()

    return bin_num[index] == counters[index].most_common(1)[0][0] if (sorted[0][1] != sorted[1][1]) else  (bin_num[index] =='1')

    #a number passes this criteria if the given bit is the least common
def co2_criteria(bin_num,index):

    counters = get_bit_counts(candidates)
    
    #there is a gotcha here too. Instead of simply returning true if it has the least common value, we have to check
    #to see if there is a tie. In that case, we only return true if there is a 0 here
    sorted = counters[index].most_common()

    return bin_num[index] == counters[index].most_common()[-1][0]  if (sorted[0][1] != sorted[1][1]) else  (bin_num[index] =='0')


#find our oxygen_rating
candidates = inputs
for i in range(bin_len):
    candidates = list(filter(lambda x: oxygen_criteria(x,i),candidates))
    if len(candidates) == 1:
        break
oxygen_rating = candidates[0]

#repeat to find our co2_rating
candidates = inputs
for i in range(bin_len):
    candidates = list(filter(lambda x: co2_criteria(x,i),candidates))
    if len(candidates) == 1:
        break
co2_rating = candidates[0]

#print the result
print(int(oxygen_rating,2)*int(co2_rating,2))