with open("Day 4/day4 input.txt") as f:
    inputs = f.read().splitlines()



#we will create a simple helper class to keep things neater


class bingo_board:

    #we could store the data as either 1D or 2D arrays, seperately or as tuples. It doesn't really matter
    #in this application. Here we chose 1D simply so we can slice things for easy row/column access



    #the numbers on each tile
    board_values = []
    #whether that number has been marked off
    board_marks = []
    #we assume the board is square
    board_width = 5

    is_complete = False

    #get a list of a given row by its y index
    def row(self,index, get_marks = False):

        if get_marks:
            return self.board_marks[index*self.board_width : index*self.board_width+self.board_width] if index < self.board_width else []  
        else:
            return self.board_values[index*self.board_width : index*self.board_width+self.board_width] if index < self.board_width else []

    #get a list of a given column by its x index
    def col(self,index, get_marks = False):

        if get_marks:
            return self.board_marks[index::self.board_width] if index < self.board_width else []
        else:
            return self.board_values[index::self.board_width] if index < self.board_width else []

    def check_for_winner(self):
        winning_mark = "1"
        for i in range(self.board_width):
            row = self.row(i,True)
            #you could add all the values and check that they are equal to the width
            #but this way we can check they are all the same regardless of what value we may in 'future' use

            #if a winner is found, we return true
            if all(x == winning_mark for x in row):
                return True

        #no winning row found, check columns   

        for i in range(self.board_width):
            col = self.col(i,True)

            #if a winner is found, we return true
            if all(x == winning_mark for x in col):
                return True

        #no winner found
        return False

    #mark a number if it exists
    def mark_number(self,num):
        try:
            #since we stored the arrays as flat, we can simply index them directly
            ind = self.board_values.index(num)
            self.board_marks[ind] = "1"

            if self.check_for_winner():
                self.is_complete = True
        except ValueError:
            return

    def calc_score(self,winning_num):
        unmarked = list(filter(lambda x: (self.board_marks[self.board_values.index(x)] == "0"),self.board_values))
        unmarked = list(map(int,unmarked))

        score = sum(unmarked)*int(winning_num)
        return score



#got to do some data wrangling before we can begin.

random_order = inputs[0].split(",")

#get our boards
def find_boards(data, delimiter):
    raw_inputs = []
    boards = []

    #get all the board values
    end_index = 0
    while True:
        try:
            start_index = data.index(delimiter,end_index)
        except ValueError:
            break

        try:
            end_index = data.index(delimiter, start_index+1)
        except ValueError:
            break
        raw_inputs.append(data[start_index + 1: end_index])




    #now we create a board for each set of data
    for b in raw_inputs:
        new_board = bingo_board()

        #the raw data needs some modification, convert it to discreet valeus 
        for i in range(len(b)):
            b[i] = b[i].split()

        #flatten the data
        new_board.board_values = [n for sub in b for n in sub]
        new_board.board_marks = ["0"]*(len(b)*len(b))
    
        boards.append(new_board)

    return boards

boards = find_boards(inputs,'')


#now we can begin our puzzle

#part one - find the first winning board
def play_bingo():
    for draw in random_order:
        for board in boards:
            board.mark_number(draw)

            #winner found, return the board and the number that was just drawn
            if board.is_complete:
                return (board,draw)

winner = play_bingo()

#calculate the score
winning_score =winner[0].calc_score(winner[1])

print(winning_score)



#part 2

#we want to find the last winning board instead of the first
#which is simple, we will do essentially the same thing as in play_bingo
#but instead of returning early we will just overwrite the winner until the end

#first, we reset the boards

for board in boards:
    board.board_marks = ["0"]*(board.board_width*board.board_width)


last_winner = None
for draw in random_order:
        for board in boards:

            #ignore already won boards
            if board.is_complete:
                continue
            board.mark_number(draw)

            #winner found, return the board and the number that was just drawn
            if board.is_complete:
                last_winner = (board,draw)

#calculate the score
winning_score =last_winner[0].calc_score(last_winner[1])

print(winning_score)