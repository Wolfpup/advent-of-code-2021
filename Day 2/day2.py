with open("Day 2/day2 input.txt") as f:
    directions = f.read().splitlines()

 
#this could all be done without classes and just inline but let's keep it neat incase we need to do this stuff in future days

class Submarine:
    
    #current position of the submarine, represented as 'horizontal position','depth'
    current_pos = [0,0]
    #current aim of the sub
    aim =0


    #moving for part one
    def move_old(self,command):
        #tokenise the command
        input = command.split()
        #convert to int
        input[1] = int(input[1])
        #we can easily convert the incoming command to numerical values and just add the results
        delta =[1 if input[0] == 'forward' else 0,0 if input[0] == 'forward' else (1 if input[0] == 'down' else -1)]
        delta = [val*input[1] for val in delta]

        #add the position deltas to current position
        self.current_pos = [a+b for a,b in zip(self.current_pos,delta)]

    #this one is ironically simpler than the previous
    def move_aim(self,command):
        #tokenise the command
        input = command.split()
        #convert to int
        input[1] = int(input[1])

        if input[0] =="forward":

            #increase horizontal by input
            self.current_pos[0] += input[1]

            #increase depth by aim*input
            self.current_pos[1]+=self.aim*input[1]
        else:
            #increase or decrease aim accordingly
            self.aim += (1 if input[0] =="down" else -1)* input[1]


#part one- multiply final horizontal position by final depth
sub = Submarine()

for directionpair in directions:
    sub.move_old(directionpair)

print(sub.current_pos[0]*sub.current_pos[1])


#part two 
sub = Submarine()

for directionpair in directions:
    sub.move_aim(directionpair)

print(sub.current_pos[0]*sub.current_pos[1])