with open("Day 9/Day9 input.txt") as f:
    inputs = f.read().splitlines()



#we will organise our data as a 2D array.
#not that since we read in line-wise, the indices are [y][x]
heightmap = []
for row in inputs:
    heightmap.append(list(row))

#so we will transpose them for our own convenience, even though
#this is not neccessary, it makes it more sense to me to use [x][y] instead
heightmap = [list(x) for x in zip(*heightmap)]


def neighbours(array,pos):

    neighbours = []
    #imagine the cells are arranged thus:

    #   0   1   2
    #   3   4   5
    #   6   7   8

    #with 4 being the pos we are checking
    for dir in [1,3,5,7]:
        #ignoring the middle one
        if(dir==4): continue
        #neighbour indices, unbounded
        n_row = pos[0]+ (int(dir%3)-1)
        n_col = pos[1] + (int(dir/3)-1)

        #get the array if it's valid.
        n = array[n_row][n_col] if (n_row >=0 and n_row < len(array)) and (n_col>=0 and n_col < len(array[0]) ) else None

        #append valid values
        neighbours+= [n] if n is not None else []

    
    return neighbours


#determine if a point is a low point
def is_lowpoint(array,pos):

    #get the neighbours
    ns = neighbours(array,pos)
    #this is a low point if any of the neighbours are higher
    return all(x > array[pos[0]][pos[1]] for x in ns)

#get the low points
low_points = []
for x in range(len(heightmap)):
    for y in range(len(heightmap[0])):
        if is_lowpoint(heightmap,[x,y]):
            low_points.append([x,y])

#and their corresponding risks
risks = [1 + int(heightmap[x[0]][x[1]]) for x in low_points]

print(sum(risks))


