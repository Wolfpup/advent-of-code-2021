with open("Day 7/Day7 input.txt") as f:
    inputs = f.read().split(",")
import math

horizontals = list(map(int,inputs))

#find the cost of moving to a given position
def fuel_cost(pos,target):
    return abs(pos-target)

#the cost of moving to a given position given the new movement costs
def fuel_cost_2(pos,target):
    #calculate the number of 'steps' needed to get to that position
    steps = abs(pos-target)
    #maths for summing consecutive numbers
    return (steps*(steps+1))/2

target = -1
min_cost = math.inf
for i in range(max(horizontals)):
    #calculate the total fuel cost and see if it's cheaper
    total =sum(list(map(lambda x: fuel_cost_2(x,i),horizontals)))
    if total < min_cost:
        target = i
        min_cost = total


print(min_cost)
