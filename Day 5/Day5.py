with open("Day 5/day5 input.txt") as f:
    inputs = f.read().splitlines()

#this is an interesting puzzle. We could go the mathematical route using line segment
#intersections, something often used in video games and 3D applications. However, this
#time I'm going to suggest a grid based solution since it would be slightly simpler.


class grid:

    def __init__(self,x_width,y_width):
        self.y_width = y_width+1
        self.x_width = x_width+1
        self.data = [0]*(self.x_width*self.y_width)
        self.stop = False


    #the heavy lifting of this puzzle. 
    def add_line(self,start,end):
        #figure out if we're moving diagonally, ie. neither horizontally or vertically
        is_diagonal = True if (start[0] != end[0]) and (start[1]!=end[1]) else False
        is_horizontal = True if start[0] != end[0] and is_diagonal == False else False
        #slice the array depending on direction
        if is_diagonal:

            #create a slop that tells us which way to move
            slope_X = 1 if start[0] < end[0] else -1
            slope_y = 1 if start[1] < end[1] else -1
            slope = (slope_X,slope_y)

            #start at the beginning
            ind = start
            #stop one step after the end since we're doing a do...while
            stop = (end[0]+slope[0],end[1]+slope[1])
            while True:
                self.data[ind[0]+(self.y_width*ind[1])]+=1
                ind = (ind[0]+slope[0],ind[1]+slope[1])

                if(ind == stop):
                    break




        elif is_horizontal:

            xMin =min(start[0],end[0])
            xMax = max(start[0],end[0])

            #slice the array to get the values we want

            for i in range(xMin+(self.y_width*start[1]),xMax+(self.y_width*start[1])+1):
                self.data[i] +=1
        else:

            yMin =min(start[1],end[1])
            yMax = max(start[1],end[1])
            
            for y in range(yMin,yMax+1):
                self.data[start[0]+(self.y_width*y)]+=1

            
        

#get pairs of our lines in the form of (x1,y1),(x2,y2)
pairs = []
for i in inputs:
    stripped =i.split(" -> ")
    split = [tuple(map(int,i.split(","))) for i in stripped]
    pairs.append(split)
    

#find our largest y and x values
max_x = 0
max_y = 0

for pair in pairs:
    max_x = max(max_x,pair[0][0])
    max_x = max(max_x,pair[1][0])

    max_y = max(max_y,pair[0][1])
    max_y = max(max_y,pair[1][1])
    
#create a grid of those dimensions
v_grid = grid(max_x,max_y)

#for part one, we only consider horizontal or vertical lines
straight_pairs = list(filter(lambda p: (p[0][0] == p[1][0]) or (p[0][1] == p[1][1]),pairs))

#add all the lines to our grid
for pair in straight_pairs:
    v_grid.add_line(pair[0],pair[1])

#we can simply count the number of times we are above one
print(sum(i > 1 for i in v_grid.data))

#for part two, we need all the lines

#reset the grid
v_grid = grid(max_x,max_y)
v_grid.stop = True
for pair in pairs:
    v_grid.add_line(pair[0],pair[1])


#we can simply count the number of times we are above one
print(sum(i > 1 for i in v_grid.data))